# League of Eevees

## Context of the project

**League of Eevees** (LoE) is originally a school project. It has been developped by a team of six in 2018, during the first year of [our Master's Degree](http://mathsinfo.univ-tlse2.fr/accueil/formations/master-ice/) in Computer Sciences.

We had to use the SCRUM agile method. The development was split in three short sprints, so we had a very limited time to realize the whole project.

## Goals

We have to build a "**tool-supported environment for domain-specific
data querying, analysis and monitoring**". This tool had to include:
- a Domain Specific Language (DSL) for data querying, analysis and visualization,
- a front-end for monitoring (real-time, and possibly interactive), and possibly prediction (interpolation, ML).

To answer this rather abstract problematic, we chose to build a solution that would allow a League of Legends (LoL) player to query information about its games: average of kills, percentage of victory per champion, etc. As a front-end, we decided to create a web interface so that anyone with an internet connection would be able to access it.

Concretely, one would be able to:
1. Connect to our website.
2. Write a query using a tailor DSL.
3. Visualize the result of the query in a diagram.

> **Did you know ?** The name _League of Eevees_ is a contraction of _League of Legends_ and _Eevees_, a Pokemon of which one of the team members is really fond of.

## Implementation

### Documentation

The full documentation of this project can be found in [our Wiki](https://gitlab.com/Eevee-IDM/league-of-eevees-wiki/wikis/home).

### Tools

To accomplish this project, we relied on Continuous Integration (CI) and used many tools, among which:
- [Taiga](https://tree.taiga.io/project/kazejiyu-idm/),
- [Jenkins](http://ip89.ip-137-74-154.eu),
- [SonarQube](http://ip88.ip-137-74-154.eu/projects?sort=-analysis_date).

All the tools can be accessed from the [Start Page](http://ip63.ip-178-33-252.eu).

### Architecture

To ease developments, the project has been split up into three repositories:

- [a back-end](https://gitlab.com/Eevee-IDM/league-of-eevees-back), which is a REST server interpreting user requests, querying the required information, and sending the result to the web interface
- [a front-end](https://gitlab.com/Eevee-IDM/league-of-eevees-back), which is a web interface written in Angular 5 allowing the user to type its request and displaying the result as diagrams
- [a business part](https://gitlab.com/Eevee-IDM/league-of-eevees-business), which defines both a meta-model of the a user request and the Domain Specific Language (DSL) that can be used to type a request

The back-end has been developed in Java, whereas the front-end relies on TypeScript.

## Team members

The project has been realized by:
- [Léo Calvis](https://gitlab.com/Sarenya)
- [Emmanuel Chebbi](https://gitlab.com/KazeJiyu)
- [François-Marie d'Aboville](https://gitlab.com/fmdaboville)
- [Mathilde Lannes](https://gitlab.com/mathilde-lannes)
- [Alexandra Picard](https://gitlab.com/picard31)
- [Arnaud Schnapper](https://gitlab.com/arnaudSchnapper)